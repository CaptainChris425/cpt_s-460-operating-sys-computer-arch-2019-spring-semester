/*********  t.c file *********************/
//#include <ext2.h>
//#include <fcntl.h>
//#include <ext2fs/ext2_fs.h>
//#include <unistd.h>
#define BLKSIZE 1024
#define MAXLENGTH 64
#define TRK 18
#define CYL 36

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned long u32;

#include "ext2.h"
typedef struct ext2_group_desc GD;
typedef struct ext2_inode       INODE;
typedef struct ext2_dir_entry_2 DIR;    // need this for new version of e2fs

u16 NSEC = 2;

char buf1[BLKSIZE], buf2[BLKSIZE];
/*
INODE *ip;
DIR *dp;
DIR   *dp;
int fd;
*/
int getblk(u16 blk, char *buf){
    readfd( (2*blk)/CYL, ( (2*blk)%CYL)/TRK, ((2*blk)%CYL)%TRK, buf);
}

int prints(char *s)
{
// write YOUR code
    while(*s){
        putc(*s++);
    }
}

int gets(char *s)
{
// write YOUR code
    char c, *temp = s; int len = 0;
    while( (c = getc()) != '\r' && len<MAXLENGTH-1){
        *temp++ = c; putc(c); len++;
    }
    *temp = 0; return s;
}

u16 search(INODE* ip, char *name){
    DIR *dp;
    char *cp;
    int j;
    for (j=0; j < 12; j++){  // assume at most 12 direct blocks
        if ((u16)ip->i_block[j]){
           getblk((u16)ip->i_block[j], buf2);
           dp = (DIR *)buf2;
           while ((char *)dp < &buf2[BLKSIZE]){
              cp = dp->name[dp->name_len];
              dp->name[dp->name_len] = 0;
              prints(dp->name); putc(' ');
              if(!strcmp(dp->name,name)){
                  prints("\n\r");
                  return (u16)dp->inode;
               }
               dp->name[dp->name_len] = cp;
               dp = (DIR *)dp + dp->rec_len;
           }
        }
    }
    error();
}

char ans[MAXLENGTH];

main()
{
    char *cp, *name[2], filename[MAXLENGTH];
    u16 i, ino, blk, iblk;
    u32 *up;
    GD* gp;
    INODE *ip;
    DIR *dp;

/*LOGIN*/
    prints("What's your name? ");
    gets(ans);  prints("\n\r");
    if (ans[0]==0){
      prints("No name goodbye\n\r");
      return;
    }
    prints("Welcome "); prints(ans); prints("\n\r");
/*END LOGIN*/
/*START BOOTING*/
    name[0] = "boot"; name[1] = filename; //Basename is boot //file will be determined later
    prints("bootname: "); //start promt for bootfile
    gets(filename); //get name of bootfile
    if(filename[0] == 0){ name[1] = "mtx";} //if nothing assume mtx
    prints("Read GD\n");
    getc();
    getblk(2,buf1); //get Group Descriptor block
    gp = (GD*)buf1; //Cast to a GD*
    iblk = (u16)gp->bg_inode_table; //store the start of the iblock table
    getblk(iblk,buf1); //get the first iblock
    ip = (INODE *)buf1; //cast to iblock
    for (i = 0; i< 2; i++ ){
        ino = search(ip, name[i]);
        if (ino < 0){ error();}
        getblk(iblk+(ino/8), buf1);
        ip = (INODE *)buf1 + (ino % 8);
    } 
    prints("The inode of mtx is ");
    prints(ino);
    prints("\n");
    
}
  

