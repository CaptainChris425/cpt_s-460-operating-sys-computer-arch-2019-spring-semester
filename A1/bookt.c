#define BLK 1024
typedef unsigned char  u8;
typedef unsigned short u16;
typedef unsigned long  u32;
#include "ext2.h"
typedef struct ext2_group_desc  GD;
typedef struct ext2_inode       INODE;
typedef struct ext2_dir_entry_2 DIR;
u16 NSEC = 2;
char buf1[BLK], buf2[BLK];
int prints(char *s)
{while(*s){putc(*s++); }}
int gets(char *s) 
{while( (*s = getc()) != '\r'){ putc(s++); }*s = 0;}
int getblk(u16 blk, char *buf)
{readfd( blk/18, ((blk)%18)/9, ( ((blk)%18)%9)<<1, buf);}
u16 search(INODE* ip, char *name){
    char *c; int j; DIR *dp;
    for (j=0; j < 12; j++){  // assume at most 12 direct blocks
        if ((u16)ip->i_block[j]){
           getblk((u16)ip->i_block[j], buf2);
           dp = (DIR *)buf2;
           while ((char *)dp < &buf2[BLK]){
              getc();
              c = dp->name[dp->name_len];
              dp->name[dp->name_len] = 0;
              prints(dp->name); putc(' ');
              if(!strcmp(dp->name,name)){
                  prints("\n\r");
                  return (u16)dp->inode;
               }
               dp->name[dp->name_len] = c;
               dp = (char *)dp + dp->rec_len;
           }
        }
    }
    error();
}
main()
{ 
  u16    i, ino, blk, iblk;
  char   *cp, *name[2], filename[64];
  u32 *up;
  GD *gp;
  INODE *ip;
  DIR *dp;
  name[0] = "boot"; name[1] = "mtx"; 
  prints("read GD\n\r");
  getblk(2, buf1);
  gp = (GD*)buf1;
  iblk = (u16)gp->bg_inode_table; 
  prints("inode_blk="); putc(iblk+'0'); prints("\n\r"); 
  prints("read inodes begin block\n\r");
  getblk(iblk,buf1);
  ip = (INODE *)buf1 + 1;
  prints("read root DIR\n\r");
  for (i = 0; i< 2; i++ ){
    ino = search(ip, name[i]) - 1;
    if (ino < 0){
         error();
    }
    getblk(iblk+(ino/8), buf1);
    ip = (INODE *)buf1 + (ino % 8);
  } 
  prints("inode_blk="); putc(ino+'0'); prints("\n\r");
  if ((u16)ip->i_block[12]){
    getblk((u16)ip->i_block[12], buf2);
  }

  setes(0x1000);
  for( i=0; i<12; i++){
    getblk((u16)ip->i_block[i],0);
    inces(); putc('*');
  }

  if ((u16)ip->i_block[12]){
    up = (u32 *)buf2;
    while(*up++){
      getblk((u16)*up,0);
      inces(); putc('.');
    }
  }

  prints("Ready to boot?"); getc();
  return 1;
}  
