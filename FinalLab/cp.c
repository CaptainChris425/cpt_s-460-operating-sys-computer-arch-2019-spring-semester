//***********************************************************************
//                   LOGIC of cp.c file
//***********************************************************************
#include "ucode.c"

int stdin, stdout, stderr;   

main(int argc, char *argv[])
{
    printf("********CY cp********%c", '\n');
    printf("*********************%c", '\n');
    int source = 0, dest = 0;
    char buf[BLKSIZE]= { 0 };
    char finalBuf[BLKSIZE] = { 0 };
    source = open(argv[1], O_RDONLY);
    if(source < 0){
        printf("cycp: Source file was not found%c",'\n');
        return 0;
    }
    dest = open(argv[2], O_WRONLY | O_CREAT);
    if(dest < 0){
        printf("cycp: Dest file was not found%c",'\n');
        return 0;
    }

    while(read(source, buf, BLKSIZE)) {
        write(dest, buf, BLKSIZE);
    }
    close(source);
    close(dest);

    return 0;
}
