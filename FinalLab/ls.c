#include "ucode.c"

char *permString = "xwrxwrxwr-------";

main(int argc, char *argv[]) {
    STAT fstat;
    char filename[64];
    int r = 0;
    if (argc < 2) {
        strcpy(filename,".");
    }
    else {
        strncpy(filename, argv[1], strlen(argv[1]));
    }
    //stat the entered filename
    r = stat(filename, &fstat);
    if(fstat.st_mode == 16877) {
        ls_dir(filename);
    }
    else {
        ls_file(filename, filename);
    }
}

int ls_dir(char *filename) {
    char name[256], buf[1024], *cp;
    DIR *dp;
    int fd = 0;
    char temp[256];

    fd = open(filename, 0);

    read(fd,buf,1024);

    dp = (DIR *)buf;
    cp = buf;
    while(cp < buf + 1024) {
        if((strcmp(dp->name, ".") != 0) && (strcmp(dp->name, "..") != 0)) {

            strcpy(name, filename);
            strncpy(temp, dp->name, strlen(dp->name));
            temp[dp->name_len] = 0;
            strcat(name, "/");
            strcat(name, temp);
            ls_file(name, temp);

            memset(name, 0, 256);
            memset(temp, 0, 256);
        }

        cp += dp->rec_len;       // advance cp by rec_len in BYTEs
        dp = (DIR *) cp;
    }
}

int ls_file(char *filename, char *basename) {
    STAT fstat;
    int r = 0;
    r = stat(filename, &fstat);
    if(fstat.st_mode == 16877)  {
        mputc('d');
    }
    else {
        mputc('-');
    }

    for (int i = 8; i >= 0; i--) {
        if(fstat.st_mode & (1<<i)) {
            mputc(permString[i]);
        }
        else {
            mputc('-');
        }
    }

    mputc('\t');
    printf("%d", fstat.st_nlink);
    mputc('\t');
    printf("%d", fstat.st_uid);
    mputc('\t');
    printf("%d", fstat.st_gid);
    mputc('\t');
    printf("%d", fstat.st_size);
    mputc('\t');
    printk("%s", basename);
    mputc('\n');
}
