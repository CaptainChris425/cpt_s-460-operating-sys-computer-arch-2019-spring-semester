#include "ucode.c"



int isLower(char c)
{
    //a = 96, z = 123
    if ((int)c > 96 && (int)c < 123)
        return 1; 
    return 0;
}

int getFromUser(){
    //Read in from stdin instead of from a file
    char c;
    //get char from stdin
    c = getc();
    //go until there is a return entered
    while(c != '\r'){
        if(isLower(c))
           mputc(c-32);
        else
           mputc(c);
        c = getc();
    }
    mputc('\n');
    return 0;
}

int main(int argc, char *argv[]){
    //lowercase to uppercase
    printf("*******CY l2u********** %c",'\n');
    printf("*********************** %c",'\n');
    char buf[1024], newbuf[1024];
    int length = 0, fd1,fd2,i;
    //if there were no arguments
    if (argc == 1){
        //fd1 stdin fd2 stdout
        fd1 = 0;
        fd2 = 1;
        //get the input from the user
	    getFromUser();
    }
    else if (argc == 2)
    {
        //fd1 file1 fd2 stdout
        fd1 = open(argv[1], O_RDONLY);
        fd2 = 1;
    }
    else if (argc == 3)
    {
        //fd1 file1 fd2 file2
        fd1 = open(argv[1], O_RDONLY);
        fd2 = open(argv[2], O_WRONLY | O_CREAT);
    }
    
    if (fd1 < 0 || fd2 < 0)
    {
        //if either of the fd's < 0 file opening failed
        printf("cyl2u: Cannot read file(s)%c",'\n');
        return 0;
    }
    
    do
    {
        //read a block
        length = read(fd1, buf, 1024);
        //for every character read
        for (i = 0; i < length; i++)
        {
            //if its a lowecase letter
            if (isLower(buf[i]))
                //make it uppercase
                newbuf[i] = (buf[i] - 32);
            else
                newbuf[i] = buf[i];
        }
        if (fd2 != 1)
            //write the length into the fd2
            write(fd2, newbuf, length);
        else
        {
            //print out newbuf
            for (i = 0; i < length; i++)
            {
                if (newbuf[i] == '\n'){
                    mputc('\n');
                }else{
                    mputc(newbuf[i]);
                }
            }
        }        
    } while (length == 1024);
    close(fd1);
    close(fd2);
    return 0;
}

