//***********************************************************************
//                   LOGIC of grep.c file
//***********************************************************************
#include "ucode.c"

int main(int argc, char *argv[])
{
    char line[1024];
    int file = 0;
    if(argc < 2) {
        printf("cygrep: No pattern found to grep.\n");
        return 0;
    }
    if (argc > 2)
        file = open(argv[2], 0);

    if(file < 0) {
        printf("cygrep: File did not open\n");
        return 0;
    }

    int i = 0;
    while (read(file, &line[i], 1)) {
            if(line[i] == '\n' || line[i] == '\r') {
                line[i] = 0;
                if(strstr(line, argv[1])) {
                    printf("%s%c", line, "\n");
                }
                i = 0;
            }
            else {
                i++;
            }
    }

    return 0;
}


