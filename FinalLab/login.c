#include "ucode.c"

int stdin, stdout, stderr;   
char username[128], password[128];
char *tty;
int passfd;
char *tokens[84];

void tokenizeLogin(char *line)
{
  //tokenize the password file by ':'
  char *cp;
  cp = line;
  int n = 0;
  //while there is more to read
  while (*cp){
    //while we are at a : or a newline, set to null and increment pointer
       while (*cp == ':' || *cp == '\n') *cp++ = 0;
       //if cp points at something        
       if (*cp)
        //set tokens[n] to the start of the token string
           tokens[n++] = cp;
        //move cp over the string
       while (*cp != ':' && *cp != '\n' && *cp != 0) cp++;                  
       //set a null termination at the end of the token string
       if (*cp)
           *cp = 0;                   
       else
            break; 
       //increment to next token
       cp++;
  }
  //null the list out
  tokens[n] = 0;
}

main(int argc, char *argv[])   // invoked by exec("login /dev/ttyxx")
{
  //sets the starting tty to the tty entered by the invocation
  tty =  argv[1];
  char buf[1024];
  int loggedIn = 0, index = 0, nameLen = 0, passLen = 0;

  //Close stdin,stdout,stderr if they were open
  close(0); close(1); close(2);

  // open tty for stdin, stdout, stderr all the the tty
  stdin  = open(tty, 0);
  stdout = open(tty, 1);
  stderr = open(tty, 2);
  //syscall fixtty
  fixtty(tty);
  printf("cylogin: Open %s as stdin, stdout, stderr%c", tty,'\n');
  //syscall to signal 2 and catch by 1
  signal(2,1);
  //open password file
  passfd = open("/etc/passwd", 0);
  while(read(passfd, buf, 1024)); //read a block into the buf
  tokenizeLogin(buf);

  while(1){
    printf("User:");
    nameLen = gets(username);
    printf("Password:");
    passLen = gets(password);

    while(tokens[index]) {
        //go through all the tokens and see if the username and password match
        if(!strncmp(username, tokens[index], nameLen) && !strncmp(password, tokens[index+1], passLen)) {
          //if the username AND the password match then loggedIn is true
            loggedIn = 1;
            break;
        }

        index++;
    }

    if(loggedIn) {
        printf("cylogin: Logged into user : %s %c", username, '\n');
        //set uid
        chuid(atoi(tokens[index+2]), atoi(tokens[index+3]));
        //chdir to home dir
        chdir(tokens[index+5]);
        //close password file
        close(passfd);
        //execute startup program
        exec(tokens[index+6]);
        return 0;
    }
    //loggedIn was false
    printf("cylogin: Invalid login, try again%c",'\n');
    index = 0;
  }
}
