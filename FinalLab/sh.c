char cmdLine[64];


#include "ucode.c"       /* contains utility functions */


int menu()
{
  printf("#############CY MENU ########################################%c", '\n');
  printf("#  ls       cd     pwd    cat   more    cp     mv   >   >>  #%c", '\n');
  printf("#  mkdir    rmdir  creat  rm    chmod   chown  lpr  <   |   #%c", '\n');
  printf("#############################################################%c", '\n');
}

int has_redirection(char *str, char *file)
{
    int i = 1, rv = 0;
    char tmp[64], tmp2[64];
    
    while (!rv && str[i])
    {
        //input redirection
        if (str[i] == '<')
        {
            rv = INPUTRV;
        }
        //output redirection
        else if (str[i] == '>')
        {
            if (str[i + 1] == '>')
                //>>
                rv = OUTPUTRV;
            else
                //>
                rv = APPENDRV;
        }
        
        if (rv)
        {
            //if redirect was found
            //delete the space behind it
            str[i - 1] = 0;
            //delete the rv
            str[i++] = 0;
            //if rv is >> must delete the second >
            if(rv == 2)
               str[i++] = 0;
            //if there is a space delete it
            if (str[i] == ' ')
                str[i++] = 0;
            //override file with everything after the rv
            strcpy(file, str + i);
        }
        i++;
    }
    
    return rv;
}

int has_pipe(char *str){
    //Check if a string has a pipe in it iteritvely 
    int i = 0;
    while (str[i] != 0)
        {
            if (str[i] == '|')
            {
                return 1;
                break;
            }
            i++;
        }
    return 0;
}
// starts a redirection
void do_redirection(char *str, int rv)
{
    //switch on the value of the redirect
    switch (rv)
    {
        case INPUTRV:
            close(0);
            open(str, O_RDONLY);
            break;
        case OUTPUTRV:
            close(1);
            open(str, O_WRONLY|O_CREAT);
            break;
        case APPENDRV:
            close(1);
            open(str, O_APPEND|O_WRONLY|O_CREAT);
            break;
        default:
            printf("Invalid Redirect%c", '\n');
            break;
    }
}

int tokenizeName(char *line)
{
  char *cp, temp[128];
  strcpy(temp,line);
  cp = temp;
  argc = 0;
  
  while (*cp != 0){
       while (*cp == ' ') *cp++ = 0;        
       if (*cp != 0)
           name[argc++] = cp;         
       while (*cp != ' ' && *cp != 0) cp++;                  
       if (*cp != 0)   
           *cp = 0;                   
       else 
            break; 
       cp++;
  }
  name[argc] = 0;
  return argc;
  //strcpy(line,temp);
}

int execute(char *str)
{
    int rv = 0;
    char file[64];
    
    rv = has_redirection(str, file);
    if (rv)
    {
        do_redirection(file, rv);
    }
    exec(str);
    
    return 1;
}

int left_pipe(char *str)
{
    //transforms string into left side of pipe
    // left_pipe(cat | more) = cat
    int i = 0;
    while (str[i] != '|')
        i++;
    str[i - 1] = 0;
    
    return 1;
}

int right_pipe(char *str)
{
    //transforms string into right side of pipe
    // right_pipe(cat | more) = more
    int i = 0, j = 0;
    char tmp[256];
    
    strcpy(tmp, str);
    
    while (tmp[i] != '|')
        i++;
    i += 2;
    
    while (tmp[i])
        str[j++] = tmp[i++];
        
    str[j] = 0;
    
    return 1;
}

int do_pipe(char *str)
{
    int pd[2], pid, i;
    int hasPipe = 0;
    
    //syscall to pipe
    pipe(pd);
    
    //fork the child
    pid = fork();
    
    //Child process runs left pipe
    if (!pid)
    {
        //transforms string into only the left part
        left_pipe(str);
        //closes the origional stdout
        close(1);
        //adds the second pipe as stdout
        dup2(pd[1], 1); 
        //close the first pipe
        close(pd[0]);
        //executes the left pipe
        execute(str);
    }
    //Parent process runs right pipe
    else
    {
        //transforms the string into only the right part
        right_pipe(str);
        //closes origional stdin
        close(0);
        //adds the first pipe as the stdin
        dup2(pd[0], 0);
        //closes the second pipe
        close(pd[1]);
        i = 0;
        //checks for pipe in a string
        hasPipe = has_pipe(str);
        
        if (hasPipe)
        {
            //recursively run do_pipe on right side
            do_pipe(str);
        }
        else
        {
            //close the stdout
            close(1);
            //set stdout back to the console
            open("/dev/tty0", O_WRONLY);
            //execute final part of pipe
            execute(str);
        }
    }
}
int getCatFromUser(){
    char line[128], c;
    int length = 0;
    do{
        length = gets(line);
        puts(line);
        for(int i = 0; i< length-1; i++){
            mputc(line[i]);
        }
        mputc('\n');
    } while(length>1);
  
    return 0;
}

int main (int argc, char *argv[])
{
    int stdin, stdout, stderr, i, j, pid, cid, currentpid, status, hasPipe;
    char cwd[128];
    //sh always starts with setting the stdin and out to tty0
    stdin = open("/dev/tty0" , O_RDONLY);
    stdout = open("/dev/tty0" , O_WRONLY);
    currentpid = getpid();
    printf("cysh: Enter ? or help for menu%c",'\n');
    while(1){
        //Print out the cwd on the commandline
         getcwd(cwd);
         //match linux terminal
         printf("chris.Y@CptS460:~%s$ ", cwd);
         //read commandline
         gets(cmdLine);
         //if nothing entered loop back around
         if (cmdLine[0]==0)
             continue;
         //Tokenize command line into name[0..argc-1]
         argc = tokenizeName(cmdLine);
         //Echo command line
         printf("CmdLine: %s %c", cmdLine, '\n');
         /****************************************************************
            NOTE: some commands, e.g. cd, MUST be done by sh itself.
                  OTHER commands will be done as shown below:
          ****************************************************************/
         //print menu and loop back
         if (strcmp(name[0], "?")==0 || strcmp(name[0], "help")==0){
            menu(); continue;
         }
         //print cwd and loop back
         else if (!strcmp(name[0], "pwd")){
           pwd(); continue;
         }
         else if (!strcmp(name[0], "cd"))
         {
             //Change directory and loop again
    	    printf("Change directory to : %s %c",name[1],'\n');
            if (argc > 1){
                //cd argument 1
                chdir(name[1]);
            }else{
                //no arguments cd to basedir
                chdir("/");
            }
            continue;
         }
         else if (!strcmp(name[0], "cat") && argc == 1){
             //cat without args just reads from terminal
             getCatFromUser(); continue;
         }
         else if (!strcmp(name[0], "logout"))
            //quit the sh program
             exit(0);


          /***End of simple commands***/  
         /***** fork a child to execute the command ******/
         else{
            //fork a child
    	    pid = fork();
            //if parent proccess
    	    if (pid){
    		   printf("cysh: Waits for child to die%c",'\n');
    		   pid = wait(&status);
    	    }
            //Child attempts to run the commandline
    	    else{
                i = 0;
                //checks for pipe as a symbol in the name
                while(name[i] != 0){
                    //printf("TokenCmdline[%d] = %s %c",i,name[i],'\n');
                    if(strcmp(name[i],"|") == 0){
                        hasPipe = 1;
                        break;
                    }
                    i++;
    		    }
    		    printf("Has pipe? %d %c",hasPipe,'\n');
    		    if (!hasPipe){
    		     printf("child task %d exec to %s\n", getpid(), name[0]);
    		        execute(cmdLine);
    		     printf("exec failed\n");
    		    }else{
    		        do_pipe(cmdLine);
    		    }
    	    }
        }
    }
}

