#include "ucode.c"

int printPage(int fd, int lines){
    int i,j,length;
    char c;
    //Print the entered lines
    for (i = 0; i < lines; i++){
        //print for the width of the screen
        for(j = 0; j < 80; j++){
            //read 1 and put
            length = read(fd, &c, 1);
            mputc(c);
            //cannot read anymore
            if (length < 1)
                 exit(0);
            if (c == '\n' || c == '\r')
                 break;
        }
    }
}

int main(int argc, char *argv[]){
    printf("*****CY More*****%c", '\n');
    printf("*****************%c", '\n');
    char c, tty[64];
    int fd = 0;
    if(argc == 1){ 
        //duplicates and returns it
        fd = dup(0); 
        //close 0 
        close(0); 
        gettty(tty); 
        open(tty, O_RDONLY); 
    } 
    else if(argc > 1){
        fd = open(argv[1], O_RDONLY);
    }
    if (fd < 0){
        printf("cymore: Couldnt open the files %c", '\n');
	return 0;
    }
    while(1){
        c = getc();
        switch (c)
        {
                //Return or new line print one line
                case '\r':
                case '\n':
                    printPage(fd, 1);
                    break;
                //Pressed Space print a page
                case ' ':
                    printPage(fd, 20);
                    break;
                //Pressed q to quit
                case 'q':
                    mputc('\n');
                    return 0;
                    break;
        }
    }
}

