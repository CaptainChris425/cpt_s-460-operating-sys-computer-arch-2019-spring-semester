//***********************************************************************
//                   init.c 
//***********************************************************************
#include "ucode.c"
int console;

int parent() // Process 1 code
{
    int pid, status;
    while(1){
        printf("CY INIT [%d]: wait for ZOMBIE child%c",console, '\n');
        pid = wait(&status);
        if (pid == console){
            //if the process 
            printf("CY INIT: forks a new console login%c", '\n');
            console = fork();
            if (console)
                continue;
            else
                exec("login dev/tty0");
        }
    }
}

main(){
    int in, out;
    //tty0 for rd is reading from the standard output
    in = open("/dev/tty0" , O_RDONLY);
    //tty0 for wr is writing to the standard output [stdout]
    out = open("/dev/tty0" , O_WRONLY);
    console = fork();
    printf("CY INIT: Forked a login proc [%d]%c",console, '\n');
    //parent proccess runs parent
    if (console)
        //forked proc executes the main body of the code [parent{}]
        parent();
    //child proccess runs login
    else
        //The initial process
	//executes a login process on the stdout
        exec("login /dev/tty0");
}
