#include "ucode.c"




int main(int argc, char *argv[]){
    //fd = stdin for reading from
    int fd = 0;
    char buf[BLKSIZE]= { 0 };
    if (argc > 1)
    //if there is an argument to read from then open that file
    	fd = open(argv[1], O_RDONLY);
    
    if(fd < 0){
    //if not reading from stdin and failed to open file
        printf("File %s not opened%c", argv[1], '\n');
        return 0;
    }    
    //while theres more to read from input, print
    while(read(fd, buf, BLKSIZE)) {
        printf("%s",buf);
    }
    //close the input
    close(fd);
    return 0;
}

