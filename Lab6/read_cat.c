
int read_file(){
    int fd;
    int nbytes; 
    char buf[BLKSIZE];
    printf("Enter in the fd to be read: ");
    fd = atoi(gets());
    printf("Enter in the numBytes to be read: ");
    nbytes = atoi(gets());
    if (running->fd[fd]->mode == 2){
        printf("File is not in read mode");
        return;
    }
    myread(fd, buf, nbytes);
    printf("%s\n",buf);
}



int myread(int fd, char buf[], int nbytes){
    char readbuf[BLKSIZE];
    OFT *oftp;
    MINODE *mip;
    int lbk, start, remain, avil, blk, count = 0, *indirect;
    char *cq;
    oftp = running->fd[fd];
    mip = oftp->mptr;
    oftp = running->fd[fd];
    //printf("i_size = %d\n",mip->INODE.i_size);
    printf("offset = %d\n", oftp->offset);
    avil = mip->INODE.i_size - oftp->offset; 
    cq = buf;
    while(avil && nbytes){
        lbk = oftp->offset / BLKSIZE;
        start = oftp->offset % BLKSIZE;
        
        if (lbk < 12)
            blk = mip->INODE.i_block[lbk];
        else if (lbk >= 12 && lbk < 256+12){
            get_block(mip->dev, mip->INODE.i_block[12], indirect);
            get_block(mip->dev, indirect[lbk - 12], readbuf);
        }
        else{
            //DOUBLE INDIRECT BLOCKS 
        }

        get_block(mip->dev, blk, readbuf);
        char *cp = readbuf+start;
        remain = BLKSIZE - start;
        
        while(remain){
            *cq++ = *cp++;
            oftp->offset++;
            count++;
            avil--; nbytes--; remain--;
            if (nbytes <= 0 || avil <= 0)
                break;
        } 
    }
    return count; 
}


int my_cat(char filename[]){
    printf("==========================\n");
    printf("    cat file [ %s ]\n",filename);
    printf("==========================\n");
    int ino = getino(filename);
    char mybuf[1024], dummy = 0;
    int n, fd;
    int close = 1;
    fd = open_file(filename, "R");
    printf("\nSHOULD START PRINTING NOW!!!\n");
    while(n = myread(fd,mybuf,1024)){
        mybuf[n] = 0;
        printf("%s\n",mybuf);
    }
    if (close){
        close_fd(fd);
    }

}







