#include "type.h"

/*
extern PROC *running;
extern int dev;
extern MINODE *root;
#define NFD     64
#define NOFT    64kkk
*/

OFT *oftalloc()
{
    int i;
    for(i=0;i<NOFT;i++){
        OFT *of = &oft[i];
        if (of->refCount == 0){
                of->refCount = 1;
                return of;
        }
    }
    printf("No more avaliable ofts...\n");
    return -1;
}

int truncate(MINODE * mip)
{
	int i;
	char buf[1024];
	for (i = 0; i< 12; i++){	
		if(mip->INODE.i_block[i] != 0){
			bdalloc(dev, mip->INODE.i_block[i]);
		}
	}
	//I NEED TO DEALLOCATE ALL THE INDIRECT AND DOUBLE INDIRECT BLOCKS
		//How the heck do I get them
		//ill try to get block the 12 block and then make an inode from it
		//lmao it wont work probably though
	/*
	if (mip->INODE.i_block[12] != 0){
		get_block(dev,mip->INODE.i_block[12],buf);
		DIR *dp = (DIR *)buf; 
					
	}
	*/

}

int checkIfOpen(int ino){
    //This function will find the ino in the table of open files
    //if the ino is not found it will return 0
    //if the ino is found and only open for reading it will return 0
    //if the ino is found and open for writing or append it will return 1
    int i;
    int ret = 0;
    for(i=0;i<NOFT;i++){
        //if (oft[i] == NULL) return 0;
        //if (running->fd[i] == NULL) return 0;
        OFT *of = &oft[i];
        //OFT *of = &running->fd[i];
        if(of->mptr != NULL){
        if (of->mptr->ino == ino){
            printf("%d == %d\n",  of->mptr->ino ,ino);
            if (of->mode == 1 ||of->mode == 2||of->mode == 3){
                ret = 1;
                return ret;
            }else if(of->mode ==0){
                ret = 0;
            }
        }
        }
    }
    return ret;
}

int open_file(char *file, char *cmode)
{
	int mode;
	if (strcmp("R",cmode)==0) mode = 0;
	else if (strcmp("W",cmode)==0) mode = 1;
	else if (strcmp("WR",cmode)==0) mode = 2;
	else if (strcmp("A",cmode)==0) mode = 3;
	else mode = atoi(cmode);

	//When this file gets called,
		//file = the file to be opened
		//mode = either 0|1|2|3 for R|W|RW|APPEND	
	int ino, i;
	MINODE *mip;
	if (file[0] == '/') dev = root->dev;
	else dev = running->cwd->dev;
	ino = getino(file);
	
	//Check to see if file exists
	if (ino < 0){
		//create_file(file);
        creat_file();
		ino = getino(file);
	}
	//Get the file minode and then check if it is either a reg file or a lnk file	
	mip = iget(dev,ino);
	if (!S_ISREG(mip->INODE.i_mode) && !S_ISLNK(mip->INODE.i_mode)){
		printf("The file is not a regular file, cannot open for mode %s\n",mode	);
		iput(mip);
		return 0;
	}
	//Function to check if the file is open for write already
	//if it is the function returns 1
	if (checkIfOpen(ino)){
		printf("The ino %d is already open with a write mode\n");
		iput(mip);	
		return 0;
	}
	//Allocating a free oft
	OFT *openFile = oftalloc();
	//if it returns -1 then there is no more oft's to allocate
	if (openFile < 0){
		iput(mip);
		return 0;
	}
	//update the oft values
	openFile->mode = mode;
	openFile->refCount = 1;
	openFile->mptr = mip;
	//What to do with the mode	
	switch(mode){
		//MODE = READ
		case 0: openFile->offset = 0;
				mip->INODE.i_atime = time(0);
				break;

		//MODE = WRITE (delete data in file then write from beginnning) 
		case 1: truncate(mip);
				openFile->offset = 0;
				mip->INODE.i_atime = time(0);
				mip->INODE.i_mtime = time(0);
				break;

		//MODE = READWRITE (Read from beginning or overwrite from beginning)
		case 2: openFile->offset = 0;
				mip->INODE.i_atime = time(0);
				mip->INODE.i_mtime = time(0);
				break;

		//MODE = APPEND (Write at the end of the file)
		case 3: openFile->offset = mip->INODE.i_size;
				mip->INODE.i_atime = time(0);
				mip->INODE.i_mtime = time(0);
				break;
		
		default: printf("%d is an invalid mode, valid modes are 0|1|2|3\n",mode);	
				return(-1);

	}
	
	for(i=0;i<NFD;i++){
		if (running->fd[i] == 0){
			running->fd[i] = openFile;
			break;
		}
	}
	mip->dirty = 1;
	return i;

}



int l_seek(char *cfd, char* cposition)
{
	int position = atoi(cposition);
	int fd = atoi(cfd);
	MINODE *mip;
	OFT *oftp;
	if(fd > NFD){
		printf("File descriptor to close [%d] is greater than the max fd [%d]\n",fd,NFD);
		return 0;
	}
	if(running->fd[fd] == NULL){
		printf("The specified file descriptor [%d] is not part of the current proccess' open files\n",fd);
		return 0;
	}
	oftp = running->fd[fd];
	mip = oftp->mptr;
	int maxFileSize= mip->INODE.i_size;
	if(position > maxFileSize){
		printf("Cannot move past the end of the file\n");
		return 0;
	}
	int ogPosition = oftp->offset;
	oftp->offset = position;
	return ogPosition;	
}


int pfd()
{
	int i;
	OFT *oft;
	printf("fd     mode    offset    [Dev,INODE]\n");
	printf("----  -----   -------   ----------\n");
	for (i=0;i<NFD;i++){
		if(running->fd[i] != 0){
			oft = running->fd[i];
			printf("%d        %d        %d        [%d, %d]\n",i,oft->mode,oft->offset,oft->mptr->dev,oft->mptr->ino);
		}
	}
	printf("--------------------------------------\n");
}
	
int close_file(char *cfd)
{
	int fd = atoi(cfd);
	MINODE *mip;
	OFT *oftp;
	if(fd > NFD){
		printf("File descriptor to close [%d] is greater than the max fd [%d]\n",fd,NFD);
		return 0;
	}
	if(running->fd[fd] == 0){
		printf("The specified file descriptor [%d] is not part of the current proccess' open files\n",fd);
		return 0;
	}
	oftp = running->fd[fd];
	running->fd[fd] = 0;
    oft[fd].mptr = 0;
	//compressFd();
	oftp->refCount --;
	if (oftp->refCount>0) return 0;
	mip = oftp->mptr;
	iput(mip);
	return 0;
}

int close_fd(int fd)
{
	MINODE *mip;
	OFT *oftp;
	if(fd > NFD){
		printf("File descriptor to close [%d] is greater than the max fd [%d]\n",fd,NFD);
		return 0;
	}
	if(running->fd[fd] == 0){
		printf("The specified file descriptor [%d] is not part of the current proccess' open files\n",fd);
		return 0;
	}
	oftp = running->fd[fd];
	running->fd[fd] = 0;
    oft[fd].mptr = 0;
	//compressFd();
	oftp->refCount --;
	if (oftp->refCount>0) return 0;
	mip = oftp->mptr;
	iput(mip);
	return 0;
}


int dup(char *cfd){
    int fd = atoi(cfd);
    int i;
    OFT *oftp;
    if(fd > NFD){
        printf("File descriptor to dup [%d] is greater than the max fd [%d]",fd ,NFD);
        return 0;
    }
    if (running->fd[fd] == NULL){
        printf("The specified file descriptor [%d] is not part of the current proccess' open files",fd);
        return 0;
    }
    oftp = running->fd[fd];
    oftp->refCount++;
    for (i = fd; i < NFD; i++){
        if (running->fd[i]==0){
            running->fd[i] = oftp;
            return 0;   
        }
    }
    printf("There was not an empty fd to duplicate into\n");
    return 0;
}


int dup2(char *cfd, char *cgd){
    int fd = atoi(cfd);
    int gd = atoi(cgd);
    int i;
    OFT *oftp;
    if(fd > NFD || gd > NFD){
        printf("One of the file descriptors to dup [%d,%d] is greater than the max fd [%d]",fd,gd ,NFD);
        return 0;
    }
    if (running->fd[fd] == NULL){
        printf("The specified file descriptor [%d] is not part of the current proccess' open files",fd);
        return 0;
    }
    close_file(cgd);
    oftp = running->fd[fd];
    oftp->refCount++;
    running->fd[gd] = oftp;
    return 0;
}


