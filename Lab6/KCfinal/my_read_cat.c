#include "type.h"

int read_file(){
    int fd;
    int nbytes; 
    //char rbuf[BLKSIZE];
    printf("Enter in the fd to be read: ");
    fd = atoi(gets());
    printf("Enter in the numBytes to be read: ");
    nbytes = atoi(gets());
    if (running->fd[fd]->mode == 2){
        printf("File is not in read mode");
        return;
    }
    myread(fd, buf, nbytes);
}

int myread(int fd, char *buf, int nbytes) {
    int countBytes = 0;
    MINODE *mip;
    int lblk = 0, start = 0;
    char kbuf[BLKSIZE] = { 0 };
    int remain = 0;
    char *cp;
    int *indirect;

    if(!running->fd[fd]) {
        printf("File not opened\n");
        return 0;
    }
    mip = running->fd[fd]->mptr;
    int offset = running->fd[fd]->offset;
    int avil = mip->INODE.i_size - offset;

    printf("OFFSET: %d AVIL: %d\n", offset, avil);
    while(nbytes && avil) {
        lblk = offset / BLKSIZE;
        start = offset % BLKSIZE;

        if(lblk < 12) {
            get_block(mip->dev, mip->INODE.i_block[lblk], kbuf);
        }
        else if(lblk < 12 + 256) {
            get_block(mip->dev, mip->INODE.i_block[12], indirect);
            get_block(mip->dev, indirect[lblk - 12], kbuf);
        } 
        
        cp = kbuf + start;
        remain = BLKSIZE - start;

        while(remain) {
            *buf++ = *cp++;
            offset++; countBytes++;
            remain--; avil--; nbytes--;
            if(nbytes <= 0 || avil <= 0)
                break;
        }

    }

    running->fd[fd]->offset = offset;
    return countBytes;
}

int my_cat(char filename[]) {
    int index = 0;
    char buf[BLKSIZE];
    index = open_file(filename, 0);
    if(index < 0){
        printf("File not opened\n");
    }
    while(myread(index, buf, BLKSIZE))
        printf("%s", buf);
    close_file(index);
    return 0;
}



/*
int myread(int fd, char buf[], int nbytes){
    char readbuf[BLKSIZE];
    OFT *oftp;
    MINODE *mip;
    int lbk, start, remain, avil, blk, count;
    char *cq;
    mip = oftp->mptr;
    oftp = running->fd[fd];
    avil = mip->INODE.i_size - oftp->offset; 
    cq = buf;
    while(avil && nbytes){
        lbk = oftp->offset / BLKSIZE;
        start = oftp->offset % BLKSIZE;
        
        if (lbk < 12)
            blk = mip->INODE.i_block[lbk];
        else if (lbk >= 12 && lbk < 256+12){
            //INDIRECT BLOCKS
        }
        else{
            //DOUBLE INDIRECT BLOCKS 
        }

        get_block(mip->dev, blk, readbuf);
        char *cp = readbuf+start;
        remain = BLKSIZE - start;
        
        while(remain){
            *cq++ = *cp++;
            oftp->offset++;
            count++;
            avil--; nbytes--; remain--;
            if (nbytes <= 0 || avil <= 0)
                break;
        } 
    } 
}

*/
