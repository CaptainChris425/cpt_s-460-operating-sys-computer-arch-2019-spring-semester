#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ext2fs/ext2_fs.h>
#include <string.h>
#include <libgen.h>
#include <sys/stat.h>
#include <time.h>
//#include <unistd.h>

/*
#include "util.c"
#include "balloc.c"
#include "ialloc.c"
#include "enter_name.c"
#include "init_mountRoot_quit.c"
#include "link_unlink_symlink_readlink.c"
#include "ls_cd_pwd.c"
#include "mkdir_creat.c"

*/
/*************** type.h file *********************************/
typedef unsigned char  u8;
typedef unsigned short u16;
typedef unsigned int   u32;

typedef struct ext2_super_block SUPER;
typedef struct ext2_group_desc  GD;
typedef struct ext2_inode       INODE;
typedef struct ext2_dir_entry_2 DIR;


#define BLKSIZE  1024

#define NMINODE    64
#define NOFT       64
#define NFD        10
#define NMOUNT      4
#define NPROC       2

#define FREE        0
#define READY       1

#define SUPER_MAGIC 0xEF53

#define LINK_FILE 0120777

char gpath[256]; // holder of component strings in pathname
char *name[64];  // assume at most 64 components in pathnames
int  n;

int  fd, dev;
int  nblocks, ninodes, bmap, imap, iblk, inode_start;
char line[256], cmd[32], pathname[256], file1[256], file2[256];
char buf[BLKSIZE];
SUPER *sp;
GD    *gp;
INODE *ip;
DIR   *dp;   



typedef enum bool {
    false = 0, true = 1
} Bool ;

typedef struct minode{
  INODE INODE;
  int dev, ino;
  int refCount;
  int dirty;
  int mounted;
  struct mntable *mptr;
}MINODE;

typedef struct oft{
  int  mode;
  int  refCount;
  MINODE *mptr;
  int  offset;
}OFT;

typedef struct proc{
  struct proc *next;
  int          pid;
  int          ppid;
  int          status;
  int          uid, gid;
  MINODE      *cwd;
  OFT         *fd[NFD];
}PROC;

// Mount Table structure
typedef struct mtable{
		int dev;
		int ninodes;
		int nblocks;
		int	free_blocks;
		int	free_inodes;
		int	bmap;
		int	imap;
		int	iblock;
		MINODE *mntDirPtr;
		char devName[64];
		char mntName[64];
}MTABLE;

MINODE minode[NMINODE];
MINODE *root;
PROC   proc[NPROC], *running;
OFT oft[NOFT];
/*********************************************************/
