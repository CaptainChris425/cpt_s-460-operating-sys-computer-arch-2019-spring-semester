/****************************************************************************
*                   KCW testing ext2 file system                            *
*****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ext2fs/ext2_fs.h>
#include <string.h>
#include <libgen.h>
#include <sys/stat.h>

#include "type.h"

extern MINODE minode[NMINODE];
extern MINODE *root;
extern PROC   proc[NPROC], *running;

extern char gpath[256];
extern char *name[64]; // assume at most 64 components in pathnames
extern int  n;
extern int  fd, dev;
extern int  nblocks, ninodes, bmap, imap, inode_start;
extern char pathname[256], parameter[256];


int open_file() {
    printf("Enter in filename to be opened: ");
    gets(pathname):
    kt
    MINODE *mip;
    int ino = getino(file);
    int mode = 0;
    if(ino <= 0) {
        creat_file();
        ino = getino(file);
    }
    mip = iget(dev, ino);

    if (!S_ISREG(mip->INODE.i_mode) {
        printf("%s is not a regular file\n", pathname);
        return -1;
    }

    if() {
        
    }

    OFT *otable = (OFT *)malloc(sizeof(OFT *));

    mode = atoi(parameter);

    otable->mode = mode;
    otable->minodePtr = mip;
    otable->refCount = 1;
    switch(mode){
        case 0 : oftp->offset = 0;     // R: offset = 0
                break;
        case 1 : truncate(mip);        // W: truncate file to 0 size
                oftp->offset = 0;
                break;
        case 2 : oftp->offset = 0;     // RW: do NOT truncate file
                break;
        case 3 : oftp->offset =  mip->INODE.i_size;  // APPEND mode
                break;
        default: printf("invalid mode\n");
                return(-1);
    }

    for(int i = 0; i < NFD; i++) {
        if(!running->fd[i]) {
            running->fd[i] = otable;
            printf("WE GET INDEX HERE: %d\n", i);
            printf("OFFSET: %d\n\n", otable->offset);
            return i;
        }
    }

    return -1;
}
