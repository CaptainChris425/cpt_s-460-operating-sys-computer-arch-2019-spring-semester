#include "type.h"
extern PROC *running;
extern int dev;
extern MINODE *root;

int get_fd(int ino){
	int i;
    int ret = 0;
    for(i=0;i<NOFT;i++){
        //if (oft[i] == NULL) return 0;
        //if (running->fd[i] == NULL) return 0;
        OFT *of = &oft[i];
        //OFT *of = &running->fd[i];
        if(of->mptr != NULL){
			if (of->mptr->ino == ino){
				printf("\nFound fd requested!\n");
				return i;
			}
        }
    }
    return ret;
}

int my_write(int fd, char *buf, int nbytes) {
    int countBytes = 0;
    int lblk = 0, start = 0;
    int indirect[BLKSIZE], doubleIndirect[BLKSIZE];
	char *cp, *cq = buf;
    int remain;
    char kbuf[BLKSIZE] = { 0 };
    int fileSize = 0;
    if(!running->fd[fd]) {
        printf("File cannot be opened\n");
        return 0;
    }
    MINODE *mip = running->fd[fd]->mptr;
    int offset = running->fd[fd]->offset;
    fileSize = mip->INODE.i_size;
    int blk = 0;

    printf("INO: %d, FILESIZE: %d", mip->ino, fileSize);
    while (nbytes) {
        lblk = offset / BLKSIZE;
        start = offset % BLKSIZE;
        if(lblk < 12) {
            if(!mip->INODE.i_block[lblk]) {
                mip->INODE.i_block[lblk] = balloc(mip->dev);
            }
	    blk = mip->INODE.i_block[lblk];
            get_block(mip->dev,blk , kbuf);
        }
        else if(lblk < 12 + 256) {
	    //Indirect blocks
            if(!mip->INODE.i_block[12]) {
                mip->INODE.i_block[12] = balloc(mip->dev);
                get_block(mip->dev, mip->INODE.i_block[12], indirect);
                for(int i = 0; i < BLKSIZE; i++)
			indirect[i] = 0;
		put_block(mip->dev, mip->INODE.i_block[12], indirect);
            }
            get_block(mip->dev, mip->INODE.i_block[12], indirect);
            blk = indirect[lblk - 12];
            if(blk == 0) {
		indirect[lblk - 12] = balloc(mip->dev);
		blk = indirect[lblk - 12];
	    }
            put_block(mip->dev, mip->INODE.i_block[12], indirect);
            get_block(mip->dev, blk, kbuf);
        }
	else {
            if(!mip->INODE.i_block[13]) {
                mip->INODE.i_block[13] = balloc(mip->dev);
                zero_block(mip->dev, mip->INODE.i_block[13]);
            }
            get_block(mip->dev, mip->INODE.i_block[13], indirect);
            blk = indirect[(lblk - (12+256))/256];

            if(blk == 0) {
				indirect[(lblk - (12+256))/256] = balloc(mip->dev);
                zero_block(mip->dev, mip->INODE.i_block[(lblk - (12+256))/256]);
				put_block(mip->dev, mip->INODE.i_block[13], indirect);
			}
			get_block(mip->dev, indirect[(lblk - (12+256))/256], doubleIndirect);
            blk = doubleIndirect[(lblk - (12+256))%256];

            if(blk == 0) {
                doubleIndirect[(lblk - (12+256))%256] = balloc(mip->dev);
                zero_block(mip->dev, doubleIndirect[(lblk - (12+256))%256]);
			    put_block(mip->dev, indirect[(lblk - (12+256))/256], doubleIndirect);
            }
            get_block(mip->dev, doubleIndirect[(lblk - (12+256))%256], kbuf);
        }   
	printf("LBLK: %d\n", lblk);

        cp = kbuf + start;
        remain = BLKSIZE - start;
        
        printf("CP: %s, KBUF: %s\n", cp, kbuf);
        while(remain) {
            *cp++ = *buf++;
            offset++; countBytes++;
            remain--; nbytes--;
            if (offset > fileSize) fileSize++; 
            if (nbytes <= 0) break;
        }
        printf("KBUF AFTER: %s\n", kbuf);
        put_block(mip->dev, blk, kbuf);
    }
    mip->INODE.i_size = fileSize;
    running->fd[fd]->offset = offset;
    mip->dirty  = 1;
    return countBytes;
}

int write_file(){
	char line[256];
	char newText[256];
	int fd;
	printf("\nEnter the fd:");
	gets(line);
	fd = atoi(line);
	printf("\nEnter text for file:");
	gets(newText);
	printf("\nLength is %d", strlen(newText));

	OFT *of = running->fd[fd];
	if(of->mode != 0){
		return(my_write(fd, newText, strlen(newText)));
	}
	else{
		printf("\nFile is not able to be written to!\n");
	}
}

int cp(char *src, char *dest) {
    int fd_src = 0, fd_dest = 0;
    char buf[BLKSIZE]= { 0 };
    char finalBuf[BLKSIZE] = { 0 };

    open_file(src, "R");
    open_file(dest, "W");

	fd_src = get_fd(getino(src));
	fd_dest = get_fd(getino(dest));
	printf("\n\n %d    %d   \n\n", fd_src, fd_dest);
    if(fd_src < 0){
        printf("Source not opened\n");
    }
    if(fd_dest < 0){
        printf("Destination file not opened\n");
    }

    printf("SRC: %d, DEST: %d\n", fd_src, fd_dest);
    while(myread(fd_src, buf, BLKSIZE)) {
        printf("BUF PASSED IN: %s\n", buf);
        my_write(fd_dest, buf, BLKSIZE);
    }
    close_fd(fd_src);
    close_fd(fd_dest);
	
}

void zero_block(int dev, int blk)
{
    char buf[BLKSIZE];
    memset(buf, 0, BLKSIZE);
    put_block(dev, blk, buf);
}
