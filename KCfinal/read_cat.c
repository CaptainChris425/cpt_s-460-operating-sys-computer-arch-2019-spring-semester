#include "type.h"

int read_file(){
    int fd;
    int nbytes; 
    //char rbuf[BLKSIZE];
    printf("Enter in the fd to be read: ");
    fd = atoi(gets());
    printf("Enter in the numBytes to be read: ");
    nbytes = atoi(gets());
    if (running->fd[fd]->mode == 2){
        printf("File is not in read mode");
        return;
    }
    myread(fd, buf, nbytes);
}

int myread(int fd, char *buf, int nbytes) {
    int countBytes = 0;
    MINODE *mip;
    int lblk = 0, start = 0;
    char kbuf[BLKSIZE];
    int remain = 0;
    char *cp;
    int indi[256],doubindi[256];

    if(!running->fd[fd]) {
        printf("File not opened\n");
        return 0;
    }
    mip = running->fd[fd]->mptr;
    int offset = running->fd[fd]->offset;
    int avil = mip->INODE.i_size - offset;

//    printf("OFFSET: %d AVIL: %d\n", offset, avil);
    while(nbytes && avil) {
        lblk = offset / BLKSIZE;
        start = offset % BLKSIZE;

         if (lblk < 12)
            get_block(mip->dev, mip->INODE.i_block[lblk], kbuf);
        else if (lblk >= 12 && lblk < 256+12){
            get_block(mip->dev, mip->INODE.i_block[12], indi);
            get_block(mip->dev, indi[lblk - 12], kbuf);
        }
        else{
            //DOUBLE INDIRECT BLOCKS 
            get_block(mip->dev, mip->INODE.i_block[13], indi);
            get_block(mip->dev, indi[(lblk - (12+256))/256], doubindi);
            get_block(mip->dev, doubindi[lblk - (12+256)], kbuf);
        }
        cp = kbuf + start;
        remain = BLKSIZE - start;

        while(remain) {
            *buf++ = *cp++;
            offset++; countBytes++;
            remain--; avil--; nbytes--;
            if(nbytes <= 0 || avil <= 0)
                break;
        }

    }

    running->fd[fd]->offset = offset;
    return countBytes;
}

int my_cat(char filename[]){
    printf("==========================\n");
    printf("    cat file [ %s ]\n",filename);
    printf("==========================\n");
    int ino = getino(filename);
    char mybuf[1024], dummy = 0;
    int n, fd;
    int close = 1;
    fd = open_file(filename, "R");
    while(n = myread(fd,mybuf,1024)){
        mybuf[n] = 0;
        printf("%s",mybuf);
    }
    if (close){
        close_fd(fd);
    }
    printf("\n");
}




