int argc; char *argv[32]; // assume at most 32 tokens in cmdline
int parseArg(char *line)
{
  char *cp = line; argc = 0;
  while (*cp != 0){
    while (*cp == ' ') *cp++ = 0;// skip over blanks  
    if (*cp != 0) // token start
        argv[argc++] = cp;  // pointed by argv[ ]
    while (*cp != ' ' && *cp != 0) // scan token chars
      cp++;
    if (*cp != 0)
      *cp = 0;  // end of token
    else
      break;  // end of line
    cp++;    // continue scan
  }
    argv[argc] = 0;   // argv[argc]=0
}

int printArgs(){
  int i;
  for(i=0;i<argc;i++)
    printf("Argv[%d] = %s\n", i,argv[i]);
}

int exec(char *cmdline) // cmdLine = VA in Uspace
{
    char toParse[128];
    strcpy(toParse,cmdline);    
    parseArg(toParse);
    printArgs();
    printf("Executing command %s", cmdline);
    int i, upa, usp;
    char *cp, kline[128], file[32], filename[32];
    PROC *p = running;
    strcpy(kline, cmdline); //fetch cmdline into kernal space
    //get the first token of kline as the filename
    cp = kline;
    i = 0;
    while(*cp != ' '){
        filename[i] = *cp;
        i++; cp++;
    }
    filename[i] = 0;
    file[0] = 0;
    //upa = p->pgdir[2048] & 0xFFF0000; //PA of the umode image
    upa = (char *)(p->pgdir[2048] & 0x600000); //PA of the umode image
    //Loader will return -1 if the file is non-existant or non-executable
    if (!load(filename,p))
        return -1;
    usp = upa + 0x100000 - 128;
    strcpy((char *)usp , kline);
    //fix syscall frame in kstack to return to VA=0 of new image
    for (i=2; i<14; i++)
        p->kstack[SSIZE - i] = 0;
    p->kstack[SSIZE-1] = (int)VA(0);    //return uLR = VA(0)
    return (int)p->usp; //will replace saved r0 in kstack
}
