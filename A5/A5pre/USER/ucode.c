typedef unsigned char   u8;
typedef unsigned short  u16;
typedef unsigned int    u32;

#include "string.c"
#include "uio.c"
//#include "sh.c"
//#include "init.c"

int ubody(char *name)
{
  int pid, ppid;
  char line[64];
  u32 mode,  *up;

  mode = getcsr();
  mode = mode & 0x1F;
  printf("CPU mode=%x\n", mode);
  pid = getpid();
  ppid = getppid();
  printf("Filename = %s\n",name);

  while(1){
    
    printf("This is process #%d in Umode at %x parent=%d\n", pid, getPA(),ppid);
    umenu();
    printf("input a command : ");
    ugetline(line); 
    uprintf("\n"); 
 
    if (strcmp(line, "getpid")==0)
       ugetpid();
    if (strcmp(line, "getppid")==0)
       ugetppid();
    if (strcmp(line, "ps")==0)
       ups();
    if (strcmp(line, "chname")==0)
       uchname();
    if (strcmp(line, "switch")==0)
       uswitch();
    if (strcmp(line, "sleep")==0)
       do_sleep();
    if (strcmp(line, "wakeup")==0)
        do_wakeup();
    if (strcmp(line, "wait")==0)
        do_wait();
    if (strcmp(line, "exit")==0)
        do_exit();
    if (strcmp(line, "kfork")==0)
        ufork();
    if (strcmp(line, "fork")==0)
        do_fork();
    if (strcmp(line, "exec")==0)
        do_exec();
  }
}

int ufork(){
    return syscall(9,"u1",0,0);
}

int do_fork(){
    return syscall(10,0,0,0);
}

int do_exec(){
    char cmdline[128];
    printf("Enter the cmdline to execute: ");
    ugetline(cmdline);
    syscall(11,cmdline,0,0);

}

int umenu()
{
  uprintf("-------------------------------\n");
  uprintf("getpid getppid ps chname switch sleep wakeup wait exit kfork fork exec\n");
  uprintf("-------------------------------\n");
}

int do_init(){
   //exec("init"); 
   syscall(10,0,0,0);
}

int do_sleep(){
    int event;
    printf("Enter an event value to sleep on : ");
    event = geti();
    return syscall(5,event,0,0);
}

int do_wakeup(){
    int event;
    printf("Enter an event value to wakeup : ");
    event = geti();
    return syscall(6,event,0,0);
}



int do_wait(){
    int *status;
    return syscall(7,status,0,0);
}

int do_exit(){
    int event;
    printf("Enter an event value to exit on : ");
    event = geti();
    return syscall(8,event,0,0);
}

int getpid()
{
  int pid;
  pid = syscall(0,0,0,0);
  return pid;
}    

int getppid()
{ 
  return syscall(1,0,0,0);
}

int ugetpid()
{
  int pid = getpid();
  uprintf("pid = %d\n", pid);
}

int ugetppid()
{
  int ppid = getppid();
  uprintf("ppid = %d\n", ppid);
}

int ups()
{
  return syscall(2,0,0,0);
}

int uchname()
{
  char s[32];
  uprintf("input a name string : ");
  ugetline(s);
  printf("\n");
  return syscall(3,s,0,0);
}

int uswitch()
{
  return syscall(4,0,0,0);
}


int ugetc()
{
  return syscall(90,0,0,0);
}

int uputc(char c)
{
  return syscall(91,c,0,0);
}

int getPA()
{
  return syscall(92,0,0,0);
}

int argc; char *argv[32]; // assume at most 32 tokens in cmdline
int parseArg(char *line)
{
  char *cp = line; argc = 0;
  while (*cp != 0){
    while (*cp == ' ') *cp++ = 0;// skip over blanks  
    if (*cp != 0) // token start
        argv[argc++] = cp;  // pointed by argv[ ]
    while (*cp != ' ' && *cp != 0) // scan token chars
      cp++;
    if (*cp != 0)
      *cp = 0;  // end of token
    else
      break;  // end of line
    cp++;    // continue scan
  }
    argv[argc] = 0;   // argv[argc]=0
}

