// timer register u32 offsets from base address
#define TLOAD 0x0
#define TVALUE 0x1
#define TCNTL 0x2
#define TINTCLR 0x3
#define TRIS 0x4
#define TMIS 0x5
#define TBGLOAD 0x6

typedef volatile struct timer{
	u32 *base;
	// timer's base address
	int tick, hh, mm, ss; // timer data area
	char clock[16];
}TIMER;

typedef struct tqe{
    int time;
    PROC *proc;
    struct tqe *next;
}TQE;

TQE *tq, tqe[NPROC];

volatile TIMER timer[4];

// timer structure
void timer_init()
{
    
    tq->next = 0;
    tq->time = 0;
	int i; TIMER *tp;
    for(i=0;i<NPROC;i++){
        tqe[i].next = 0;
        tqe[i].time=0;
    }
	printf("timer_init()\n");
	for (i=0; i<4; i++){
		tp = &timer[i];
		if (i==0) tp->base = (u32 *)0x101E2000;
		if (i==1) tp->base = (u32 *)0x101E2020;
		if (i==2) tp->base = (u32 *)0x101E3000;
		if (i==3) tp->base = (u32 *)0x101E3020;
		*(tp->base+TLOAD) = 0x0;
		// reset
		*(tp->base+TVALUE)= 0xFFFFFFFF;
		*(tp->base+TRIS) = 0x0;
		*(tp->base+TMIS) = 0x0;
		*(tp->base+TLOAD) = 0x100;
		// CntlReg=011-0010=|En|Pe|IntE|-|scal=01|32bit|0=wrap|=0x66
		*(tp->base+TCNTL) = 0x66;
		*(tp->base+TBGLOAD) = 0x1C00; // timer counter value
		tp->tick = tp->hh = tp->mm = tp->ss = 0; // initialize wall clock
		strcpy((char *)tp->clock, "00:00:00");
	}
}

void timer_handler(int n) {
	int i;
	TIMER *t = &timer[n];
	t->tick++;
	// Assume 120 ticks per second
	if (t->tick==9){
    //    lock();
        printTimer();
        decrementTimer(); 
		t->tick = 0; t->ss++;
		if (t->ss == 60){
			t->ss = 0; t->mm++;
			if (t->mm == 60){
				t->mm = 0; t->hh++; // no 24 hour roll around
			}
		}
		t->clock[7]='0'+(t->ss%10); t->clock[6]='0'+(t->ss/10);
		t->clock[4]='0'+(t->mm%10); t->clock[3]='0'+(t->mm/10);
		t->clock[1]='0'+(t->hh%10); t->clock[0]='0'+(t->hh/10);
	}
	color = n;
	// display in different color
	for (i=0; i<8; i++){
		kpchar(t->clock[i], n, 70+i); // to line n of LCD
	}
	timer_clearInterrupt(n); // clear timer interrupt
    color = WHITE;
   // unlock();
}

void timer_start(int n) // timer_start(0), 1, etc.
{
	TIMER *tp = &timer[n];
	kprintf("timer_start %d base=%x\n", n, tp->base);
	*(tp->base+TCNTL) |= 0x80;
	// set enable bit 7
}


int timer_clearInterrupt(int n) // timer_start(0), 1, etc.
{
	TIMER *tp = &timer[n];
	*(tp->base+TINTCLR) = 0xFFFFFFFF;
}

void timer_stop(int n)
{
	// stop a timer
	TIMER *tp = &timer[n];
	*(tp->base+TCNTL) &= 0x7F; // clear enable bit 7
}

int t(){
    kprintf("Enter in time for timer (seconds): ");
    int seconds = geti();
    if(!seconds) return;
    kprintf("Time entered %d\n",seconds);
    tqe[running->pid].time = seconds;
    tqe[running->pid].proc = running; 
/*
    TQE *tir;
    tir->time = 50;
    tir->pid = running->pid;
    tir->next = 0;
    timerEnqueue(tir);


    TQE *tirr;
    tirr->time = 20;
    tirr->pid = running->pid;
    tirr->next = 0;
    timerEnqueue(tirr);
*/
/*
    TQE *timer;
    timer->time = seconds;
    timer->proc = running;
    timer->next = 0;
    lock();
    timerEnqueue(timer);        
    unlock();
    //printTimer();
    
    //while(1){
    //ksleep((int)&tq);
  //  }
    //unlock();
    printTimer();
*/
    timerEnqueue();        
    ksleep(running->pid);
    
}

int timertest(){
    kprintf("Enter in time for timer (seconds): ");
    int seconds = geti();
    if(!seconds) return;
    kprintf("Time entered %d\n",seconds);

    TQE *tir;
    tir->time = seconds;
    tir->proc = running;
    tir->next = 0;
    timerEnqueue(tir);


    kprintf("Enter in time for timer (seconds): ");
    seconds = geti();
    if(!seconds) return;
    kprintf("Time entered %d\n",seconds);

    TQE *tirr;
    tirr->time = 20;
    tirr->proc = running;
    tirr->next = 0;
    timerEnqueue(tirr);

    kprintf("Enter in time for timer (seconds): ");
    seconds = geti();
    if(!seconds) return;
    kprintf("Time entered %d\n",seconds);
    TQE *timer;
    timer->time = seconds;
    timer->proc = running;
    timer->next = 0;
    timerEnqueue(timer);        
    printTimer();
/*
    kprintf("Enter how many timers you would like to show: ");
    int numtimers = geti();
    int i;
    TQE *timers[128];
    for (i = 0; i < numtimers; i++){ 
        kprintf("Enter in time for timer (seconds): ");
        int seconds = geti();
        if(!seconds) return;
        kprintf("Time entered %d\n",seconds);
        TQE *timer;
        timer->time = seconds;
        timer->proc = running;
        timer->next = 0;
        timers[i] = timer;
    }
    for (i = 0; i < numtimers; i++) 
        timerEnqueue(timers[i]);        
 */   
}

int timerEnqueue(){
    TQE* t = &tqe[running->pid];
    TQE *temp = tq;
    TQE *prev = tq; 
    int inserted = 0;
    while(temp->next){
         temp = temp->next;
         if(temp->time > t->time){
            temp->time -= t->time;
            if (!inserted){
                prev->next = t;
                t->next = temp;
                inserted = 1;
            }
        }else{
          t->time -= temp->time;
        }
        prev = temp;
        if(inserted){
            //unlock();
            return;
        }
    }
    if(!inserted){
        temp->next = t;
        inserted = 1;
    }
    printTimer();
}

int printTimer(){
    int i = 0, j = 0,s=0;
    TQE *temp = 0;
    temp = tq;
color = CYAN;
    
    while(temp->next){
        i++;
        temp = temp->next;
/*
        kpchar('[',29,s+1);
        kpchar('0'+i,29,s+2);
        kpchar(']',29,s+3);
        kpchar('|',29,s+4);
        kpchar('0'+temp->time/10,29,s+5);
        kpchar('0'+temp->time%10,29,s+6);
        kpchar(' ',29,s+7);
        kpchar('0'+temp->proc->pid,i,s+8);
        kpchar(']',29,s+9);
        s += 9;
*/
        kprintf("[%d]|%d   %d|  ",i,temp->time,temp->proc->pid);
    }
color = WHITE;
    
    if(i)
        kprintf("\n");
}

int decrementTimer(){
    TQE *front = tq;
    TQE *back = tq;
    if (!front->next) return;
    front = front->next;
    front->time--;
    int i;
    if(front->time <= 0){
       back->next = front->next;
       //kwakeup((int)&tq); 
        color = YELLOW;
        kprintf("Wokeup Process %d\n", front->proc->pid);
        color = WHITE;
        kwakeup(front->proc->pid);
/*
        for(i=0;i<80; i++) kpchar(' ',29,i);
*/
    }
}
