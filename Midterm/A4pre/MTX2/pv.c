extern PROC *runnning;

int block(SEMAPHORE *s){
    printf("Blocking pid = %d", running->pid);
    running->status = BLOCK;
    enqueue(&s->queue,running);
    tswitch();
}

int signal(SEMAPHORE *s){
    PROC *p = dequeue(&s->queue);
    p->status = READY;
    printf("UnBlocking pid = %d", p->pid);
    enqueue(&readyQueue, p);
}

int P(SEMAPHORE *s){
    //int SR = int_off();
    s->value--;
    if (s->value < 0)
        block(s);
    printList("Blocking semaphore queueue ", s->queue);
    //int_on(SR);
}

int V(SEMAPHORE *s){
    //int SR = int_off();
    s->value++;
    if(s->value <= 0)
        signal(s);
    printList("UnBlocking semaphore queueue ", s->queue);
    printList("ReadyQueue ", readyQueue);
    //int_off(SR);
}

int producer(){
    char item;
    while(1){
        printf("Enter in a char to be produced: ");
        item = fgetc(stdin);
        P(&empty);
        P(&pmutex);
        buf[head++] = item;
        head %= N;
        V(&pmutex);
        V(&full); 
    }
}

int consumer(){
    char item;
    while(1){
        P(&full);
        P(&cmutex);
        item = buf[tail++];
        tail %= N;
        V(&cmutex);
        V(&empty); 
    }


}

