int exec(char *cmdline) // cmdLine = VA in Uspace
{
    printf("Executing command %s", cmdline);
    int i, upa, usp;
    char *cp, kline[128], file[32], filename[32];
    PROC *p = running;
    strcpy(kline, cmdline); //fetch cmdline into kernal space
    //get the first token of kline as the filename
    cp = kline;
    i = 0;
    while(*cp != ' '){
        filename[i] = *cp;
        i++; cp++;
    }
    filename[i] = 0;
    file[0] = 0;
    if (filename[0] != '/')
        strcpy(file, "/bin/");  //Prefix with bin if filename is relative 
    kstrcat(file,filename);
    upa = p->pgdir[2048] & 0xFFF0000; //PA of the umode image
    //Loader will return -1 if the file is non-existant or non-executable
    //if (!loadelf(file,p))
     //   return -1;
    usp = upa + 0x100000 - 128;
    strcpy((char *)usp , kline);
    //fix syscall frame in kstack to return to VA=0 of new image
    for (i=2; i<14; i++)
        p->kstack[SSIZE - i] = 0;
    p->kstack[SSIZE-1] = (int)VA(0);    //return uLR = VA(0)
    return (int)p->usp; //will replace saved r0 in kstack
}
