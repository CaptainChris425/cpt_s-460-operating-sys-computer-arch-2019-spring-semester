int tswitch();

int ksleep(int event)
{
  int sr = int_off();
//  printf("proc %d going to sleep on event=%d\n", running->pid, event);

  running->event = event;
  running->status = SLEEP;
  enqueue(&sleepList, running);
//  printList("sleepList", sleepList);
  tswitch();
  int_on(sr);
}

int kwakeup(int event)
{
  PROC *temp, *p;
  temp = 0;
  int sr = int_off();
  
  //printList("sleepList", sleepList);

  while (p = dequeue(&sleepList)){
     if (p->event == event){
	//printf("wakeup %d\n", p->pid);
	p->status = READY;
	enqueue(&readyQueue, p);
     }
     else{
	enqueue(&temp, p);
     }
  }
  sleepList = temp;
 // printList("sleepList", sleepList);
  int_on(sr);
}

int kwait(int *status)
{
    int i;
    if(!running->child){
        printf("The proccess has no children return -1\n");
        return -1;
    }
    while(1){
        for (i = 0; i<NPROC; i++){
            if(proc[i].status == ZOMBIE && proc[i].ppid == running->pid){
                *status = proc[i].exitCode;
                proc[i].status = FREE;
                running -> child = running->child->child;
                printf("Buried zombie proccess %d\n", proc[i].pid);
                enqueue(&freeList, &proc[i]);
                return proc[i].pid;
            }
        }
        ksleep(running->pid);
    }
    
}

int kexit(int exitValue)
{
  printf("proc %d in kexit(), value=%d\n", running->pid, exitValue);
  running->exitCode = exitValue;
  running->status = ZOMBIE;
  tswitch();
}


  
