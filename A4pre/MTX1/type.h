/*********** type.h file ************/
#define NPROC    9          // number of PROCs
#define SSIZE 1024          // stack size = 4KB

// PROC status
#define FREE     0          
#define READY    1
#define SLEEP    2
#define ZOMBIE   3

typedef struct proc{
    struct proc *next;      // next proc pointer       
    int  *ksp;              // saved sp: at byte offset 4
    struct proc *parent;  
    struct proc *child;  
    struct proc *sibling;  
    int   pid;              // process ID
    int   ppid;             // parent process pid 
    int   status;           // PROC status=FREE|READY, etc. 
    int   priority;         // scheduling priority
    int event;
    int exitCode;
  int   kstack[1024];     // process execution stack                 
}PROC;
